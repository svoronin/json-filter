package com.vlashel.filter;

import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.ObjectMapper;

import static org.junit.Assert.*;

import com.vlashel.filter.exception.EmptyResultException;
import org.junit.Test;

import java.io.File;
import java.util.ArrayList;
import java.util.List;

/**
 * @author vshel
 */
public class JsonFilterServiceTest {

    private JsonFilterService filterService = new JsonFilterService();
    private JsonNode objectData;
    private JsonNode arrayData;


    public JsonFilterServiceTest() throws Exception {
        ObjectMapper mapper = new ObjectMapper();
        objectData = mapper.readValue(new File(getClass().getResource("/object-data.json").getPath()), JsonNode.class);
        arrayData = mapper.readValue(new File(getClass().getResource("/array-data.json").getPath()), JsonNode.class);
    }

    @Test
    public void test1() {
        List<String> paths = new ArrayList<>();
        paths.add("company.title");

        JsonNode filtered = filterService.filter(paths, objectData);

        assertFalse("Missing Node", filtered.path("company").isMissingNode());
        assertFalse("Missing Node", filtered.path("company")
                .path("title").isMissingNode());
    }

    @Test
    public void test2() {
        List<String> paths = new ArrayList<>();
        paths.add("company.head.name");

        JsonNode filtered = filterService.filter(paths, objectData);

        assertFalse("Missing Node", filtered.path("company.head").isMissingNode());
        assertFalse(filtered.path("company.head")
                .path("name").isMissingNode());
    }

    @Test
    public void test3() {
        List<String> paths = new ArrayList<>();
        paths.add("company.head.address.country");

        JsonNode filtered = filterService.filter(paths, objectData);

        assertFalse("Missing Node", filtered.path("company.head.address").isMissingNode());
        assertFalse("Missing Node", filtered.path("company.head.address")
                .path("country").isMissingNode());
    }

    @Test
    public void test4() {
        List<String> paths = new ArrayList<>();
        paths.add("company.head.address[country,city]");

        JsonNode filtered = filterService.filter(paths, objectData);

        assertFalse("Missing Node", filtered.path("company.head.address").isMissingNode());

        assertFalse(filtered.path("company.head.address")
                .path("country").isMissingNode());
        assertFalse("Missing Node", filtered.path("company.head.address")
                .path("city").isMissingNode());
    }

    @Test
    public void test5() {
        List<String> paths = new ArrayList<>();
        paths.add("company.employees.name");

        JsonNode filtered = filterService.filter(paths, objectData);

        assertFalse("Missing Node", filtered.path("company.employees").isMissingNode());
        JsonNode node = filtered.path("company.employees");
        assertEquals("Wrong size", node.size(), 2);
        assertFalse(node.get(0).path("name").isMissingNode());
        assertFalse(node.get(1).path("name").isMissingNode());
    }

    @Test
    public void test6() {
        List<String> paths = new ArrayList<>();
        paths.add("company.employees.tasks.title");

        JsonNode filtered = filterService.filter(paths, objectData);

        assertFalse("Missing Node", filtered.path("company.employees.tasks").isMissingNode());
        JsonNode node = filtered.path("company.employees.tasks");
        assertEquals("Wrong size", node.size(), 2);
        assertEquals("Wrong size", node.get(0).size(), 3);
        assertEquals("Wrong size", node.get(1).size(), 2);
        assertFalse(node.get(0).get(0).path("title").isMissingNode());
        assertFalse(node.get(0).get(1).path("title").isMissingNode());

        assertFalse(node.get(0).get(2).path("title").isMissingNode());
        assertFalse(node.get(1).get(0).path("title").isMissingNode());
        assertFalse(node.get(1).get(1).path("title").isMissingNode());
    }

    @Test
    public void test7() {
        List<String> paths = new ArrayList<>();
        paths.add("company.employees.tasks[title,score]");

        JsonNode filtered = filterService.filter(paths, objectData);

        assertFalse("Missing Node", filtered.path("company.employees.tasks").isMissingNode());
        JsonNode node = filtered.path("company.employees.tasks");
        assertEquals("Wrong size", node.size(), 2);
        assertEquals("Wrong size", node.get(0).size(), 3);

        assertEquals("Wrong size", node.get(1).size(), 2);
        assertFalse(node.get(0).get(0).path("title").isMissingNode());
        assertFalse(node.get(0).get(1).path("title").isMissingNode());
        assertFalse(node.get(0).get(2).path("title").isMissingNode());
        assertFalse(node.get(0).get(0).path("score").isMissingNode());
        assertFalse(node.get(0).get(1).path("score").isMissingNode());

        assertFalse(node.get(0).get(2).path("score").isMissingNode());
        assertFalse(node.get(1).get(0).path("title").isMissingNode());
        assertFalse(node.get(1).get(1).path("title").isMissingNode());
        assertFalse(node.get(1).get(0).path("score").isMissingNode());
        assertFalse(node.get(1).get(1).path("score").isMissingNode());
    }

    @Test
    public void test8() {
        List<String> paths = new ArrayList<>();
        paths.add("id.hash");

        JsonNode filtered = filterService.filter(paths, objectData);

        assertFalse("Missing Node", filtered.path("id").isMissingNode());
        assertFalse("Missing Node", filtered.path("id").path("hash").isMissingNode());
    }

    @Test
    public void test9() {
        List<String> paths = new ArrayList<>();
        paths.add("[id,company]");

        JsonNode filtered = filterService.filter(paths, objectData);

        assertFalse("Missing Node", filtered.path("id").isMissingNode());
        assertFalse("Missing Node", filtered.path("company").isMissingNode());
    }

    @Test
    public void test10() {
        List<String> paths = new ArrayList<>();
        paths.add("name");

        JsonNode filtered = filterService.filter(paths, arrayData);
        assertFalse(filtered.get(0).get(0).get(0).path("name").isMissingNode());
        assertFalse(filtered.get(0).get(0).get(1).path("name").isMissingNode());
        assertFalse(filtered.get(0).get(1).get(0).path("name").isMissingNode());
        assertFalse(filtered.get(0).get(1).get(1).path("name").isMissingNode());

        assertFalse(filtered.get(1).get(0).get(0).path("name").isMissingNode());
        assertFalse(filtered.get(1).get(0).get(1).path("name").isMissingNode());
        assertFalse(filtered.get(1).get(1).get(0).path("name").isMissingNode());
        assertFalse(filtered.get(1).get(1).get(1).path("name").isMissingNode());

    }

    @Test
    public void test11() {
        List<String> paths = new ArrayList<>();
        paths.add("values.data.id");

        JsonNode filtered = filterService.filter(paths, arrayData);

        assertFalse(filtered.get(0).get(0).get(0).path("values.data").isMissingNode());
        assertFalse(filtered.get(0).get(0).get(0).path("values.data").get(0).path("id").isMissingNode());
    }

    @Test(expected = EmptyResultException.class)
    public void test12() {
        List<String> paths = new ArrayList<>();
        paths.add("none");

        filterService.filter(paths, arrayData);
    }

    @Test
    public void test13() {
        List<String> paths = new ArrayList<>();
        paths.add("info.context.name");

        JsonNode filtered = filterService.filter(paths, objectData);

        assertEquals(filtered.path("info.context").size(), 2);

    }

    @Test
    public void test14() {
        List<String> paths = new ArrayList<>();
        paths.add("info.context.surname");

        JsonNode filtered = filterService.filter(paths, objectData);

        assertEquals(filtered.path("info.context").size(), 1);
        assertFalse(filtered.path("info.context").get(0).path("surname").isMissingNode());
    }

    @Test(expected = EmptyResultException.class)
    public void test15() {
        List<String> paths = new ArrayList<>();
        paths.add("none");

        filterService.filter(paths, objectData);
    }

    @Test(expected = EmptyResultException.class)
    public void test16() {
        List<String> paths = new ArrayList<>();
        paths.add("values.data.none");

        filterService.filter(paths, arrayData);
    }

    @Test(expected = EmptyResultException.class)
    public void test17() {
        List<String> paths = new ArrayList<>();
        paths.add("company.none");

        filterService.filter(paths, objectData);
    }

    @Test(expected = EmptyResultException.class)
    public void test18() {
        List<String> paths = new ArrayList<>();
        paths.add("info.none");

        filterService.filter(paths, objectData);
    }

    @Test
    public void test19() {
        List<String> paths = new ArrayList<>();
        paths.add("info.context");
        paths.add("company.title");

        JsonNode node = filterService.filter(paths, objectData);
        assertEquals(node.size(), 2);
    }
}
