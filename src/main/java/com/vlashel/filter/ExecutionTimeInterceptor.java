package com.vlashel.filter;

import org.aopalliance.intercept.MethodInterceptor;
import org.aopalliance.intercept.MethodInvocation;

/**
 * @author vshel
 */
public class ExecutionTimeInterceptor implements MethodInterceptor {
    @Override
    public Object invoke(MethodInvocation invocation) throws Throwable {
        double t1 = System.nanoTime() / 1_000_000d;
        try {
            return invocation.proceed();
        } finally {
            double t2 = System.nanoTime() / 1_000_000d;
            System.out.println("Method call of '" + invocation.getMethod().getDeclaringClass() +
                    "#" + invocation.getMethod().getName() + "'" +
                    " took: " + (t2 - t1) + " milliseconds");
        }
    }
}
