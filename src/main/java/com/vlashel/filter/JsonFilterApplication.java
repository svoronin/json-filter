package com.vlashel.filter;

import com.google.inject.AbstractModule;
import com.google.inject.Guice;
import com.google.inject.Injector;
import com.google.inject.matcher.Matchers;
import io.dropwizard.Application;
import io.dropwizard.setup.Environment;

import java.util.regex.Matcher;


/**
 * @author vshel
 */
public class JsonFilterApplication extends Application<JsonFilterConfiguration> {

    public static void main(String[] args) throws Exception {
        args = new String[2];
        args[0] = "server";
        args[1] = JsonFilterApplication.class.getResource("/config.yml").getPath();
        new JsonFilterApplication().run(args);
    }

    @Override
    public void run(JsonFilterConfiguration configuration, Environment environment) {
        Injector injector = Guice.createInjector(new AbstractModule() {
            @Override
            protected void configure() {
                bindInterceptor(Matchers.any(), Matchers.annotatedWith(Timed.class), new ExecutionTimeInterceptor());
            }
        });
        JsonFilterResource filterResource = injector.getInstance(JsonFilterResource.class);

        environment.jersey().register(filterResource);
    }

    @Override
    public String getName() {
        return "Json Filter";
    }

}
