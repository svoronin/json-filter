package com.vlashel.filter.exception;

import com.fasterxml.jackson.databind.node.JsonNodeFactory;
import com.fasterxml.jackson.databind.node.ObjectNode;
import com.fasterxml.jackson.databind.node.TextNode;

import javax.ws.rs.WebApplicationException;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;

/**
 * @author vshel
 */
public class InvalidPathException extends WebApplicationException {
    @Override
    public Response getResponse() {
        ObjectNode object = JsonNodeFactory.instance.objectNode();
        TextNode text = JsonNodeFactory.instance.textNode("Invalid path");
        object.set("error", text);
        return Response.status(Response.Status.BAD_REQUEST).entity(object).type(MediaType.APPLICATION_JSON).build();
    }
}
