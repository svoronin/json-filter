package com.vlashel.filter;


import com.fasterxml.jackson.databind.JsonNode;
import com.google.inject.Inject;
import com.jayway.jsonpath.JsonPath;
import com.jayway.jsonpath.ReadContext;
import com.vlashel.filter.exception.JsonPathException;

import javax.ws.rs.*;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import java.util.List;

/**
 * @author vshel
 */
@Path("/")
@Produces(MediaType.APPLICATION_JSON)
@Consumes(MediaType.APPLICATION_JSON)
public class JsonFilterResource {
    @Inject
    private JsonFilterService filteringService;


    @POST
    @Path("/json-path-filter")
    @Timed
    public Response filterJsonPath(@QueryParam("path") String filterCriteria, String requestJson) {
        Object response;
        try {
            ReadContext context = JsonPath.parse(requestJson);
            response = context.read(filterCriteria);
        } catch (Exception e) {
            throw new JsonPathException(e.getMessage());
        }
        return Response.status(Response.Status.ACCEPTED).entity(response).build();
    }

    @POST
    @Path("/filter")
    @Timed
    public Response filterJson(@QueryParam("path") List<String> paths, JsonNode requestJson) {
        JsonNode response = filteringService.filter(paths, requestJson);
        return Response.status(Response.Status.ACCEPTED).entity(response).build();
    }
}
