package com.vlashel.filter;

import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.node.*;
import com.vlashel.filter.exception.EmptyResultException;
import com.vlashel.filter.exception.InvalidPathException;
import com.vlashel.filter.exception.NoFilteringCriteriaException;
import org.apache.commons.lang3.StringUtils;

import java.util.*;

/**
 * @author vshel
 */
public class JsonFilterService {

    public JsonNode filter(List<String> paths, JsonNode root) {
        Map<Deque<String>, List<String>> graphs = new LinkedHashMap<>();
        for (String path : paths) {
            mapGraphToFilteredFields(path, graphs);
        }

        JsonNode result = traverse(graphs, root);

        if (result.size() == 0) {
            throw new EmptyResultException();
        }
        return result;
    }

    private JsonNode traverse(Map<Deque<String>, List<String>> graphs, JsonNode root) {
        JsonNode result;
        if (root.isArray()) {
            result = JsonNodeFactory.instance.arrayNode();
            for (JsonNode node : root) {
                if (node.isArray()) {
                    JsonNode nodeResult = traverse(new LinkedHashMap<>(graphs), node);
                    if (nodeResult.size() > 0) {
                        ((ArrayNode) result).add(nodeResult);
                    }
                } else {
                    ObjectNode nodeResult = JsonNodeFactory.instance.objectNode();
                    for (Map.Entry<Deque<String>, List<String>> graph : graphs.entrySet()) {
                        // ++ support for root level filtering
                        if (graph.getKey().isEmpty()) {
                            rootLevelFilter(graph.getValue(), node, nodeResult);
                            // -- support for root level filtering
                        } else {
                            String graphPath = buildGraphPath(graph.getKey());
                            // each collection node needs its own graph to operate on
                            JsonNode graphResult = traverse(node, new LinkedList<>(graph.getKey()), graph.getValue());
                            if (graphResult.size() > 0) {
                                nodeResult.set(graphPath, graphResult);
                            }
                        }
                        if (nodeResult.size() > 0) {
                            ((ArrayNode) result).add(nodeResult);
                        }
                    }
                }
            }
            return result;
        } else {
            result = JsonNodeFactory.instance.objectNode();
            for (Map.Entry<Deque<String>, List<String>> graph : graphs.entrySet()) {
                // ++ support for root level filtering
                if (graph.getKey().isEmpty()) {
                    rootLevelFilter(graph.getValue(), root, (ObjectNode) result);
                    // -- support for root level filtering
                } else {
                    String graphPath = buildGraphPath(graph.getKey());
                    JsonNode graphResult = traverse(root, graph.getKey(), graph.getValue());
                    if (graphResult.size() > 0) {
                        ((ObjectNode) result).set(graphPath, graphResult);
                    }
                }
            }
            return result;
        }
    }

    private void rootLevelFilter(List<String> filteredFields, JsonNode root, ObjectNode result) {
        for (String field : filteredFields) {
            JsonNode propertyNode = root.get(field);
            if (propertyNode != null) {
                result.set(field, propertyNode);
            }
        }
    }

    private JsonNode traverse(JsonNode root, Deque<String> graph, List<String> filteredFields) {
        root = root.path(graph.poll());

        if (root.isArray()) {
            return traverse((ArrayNode) root, graph, filteredFields);
        } else if (root.isObject()) {
            return traverse((ObjectNode) root, graph, filteredFields);
        } else { // if root is a missing node - returns empty missing node
            return root;
        }
    }

    private JsonNode traverse(ArrayNode arrayNode, Deque<String> graph, List<String> filteredFields) {
        ArrayNode result = JsonNodeFactory.instance.arrayNode();
        if (graph.isEmpty()) {
            for (JsonNode node : arrayNode) {
                ObjectNode object = JsonNodeFactory.instance.objectNode();
                for (String field : filteredFields) {
                    JsonNode propertyNode = node.get(field);
                    if (propertyNode != null) {
                        object.set(field, propertyNode);
                    }
                }
                if (object.size() > 0) {
                    result.add(object);
                }
            }
        } else {
            for (JsonNode node : arrayNode) {
                // each collection node needs its own graph to operate on
                JsonNode nodeResult = traverse(node, new LinkedList<>(graph), filteredFields);
                if (nodeResult.size() > 0) {
                    result.add(nodeResult);
                }
            }
        }
        return result;
    }

    private JsonNode traverse(ObjectNode objectNode, Deque<String> graph, List<String> filteredFields) {
        if (graph.isEmpty()) {
            ObjectNode result = JsonNodeFactory.instance.objectNode();
            for (String field : filteredFields) {
                JsonNode propertyNode = objectNode.get(field);
                if (propertyNode != null) {
                    result.set(field, propertyNode);
                }
            }
            return result;
        } else {
            return traverse((JsonNode) objectNode, graph, filteredFields);
        }
    }

    private void mapGraphToFilteredFields(String path, Map<Deque<String>, List<String>> graphs) {

        if (path.isEmpty()
                || path.equals(".")
                || path.equals("[.]")
                || path.equals(",")
                || path.equals("[,]")
                || path.equals("[]")) {
            throw new NoFilteringCriteriaException();
        }

        List<String> filteredFields;
        Deque<String> graph;
        // ++ path parsing
        String fieldsBetweenBrackets = StringUtils.substringBetween(path, "[", "]");
        if (fieldsBetweenBrackets != null) {
            filteredFields = Arrays.asList(fieldsBetweenBrackets.split(","));
            if (filteredFields.isEmpty()) {
                throw new NoFilteringCriteriaException();
            }
            graph = new LinkedList<>(Arrays.asList(StringUtils.substringBefore(path, "[").split("\\.")));
            if (!StringUtils.substringAfter(path, "]").isEmpty()) {
                throw new InvalidPathException();
            }
            if (graph.peek().isEmpty()) { // quick fix for split returning empty string
                graph.clear();
            }
            graphs.put(graph, filteredFields);
        } else {
            String[] allFields = path.split("\\.");
            int lastElementIndex = allFields.length - 1;
            filteredFields = Arrays.asList(allFields[lastElementIndex]);
            if (filteredFields.isEmpty()) {
                throw new NoFilteringCriteriaException();
            }
            graph = new LinkedList<>(Arrays.asList(Arrays.copyOfRange(allFields, 0, lastElementIndex)));
            graphs.put(graph, filteredFields);
        }
        // -- path parsing
    }

    private String buildGraphPath(Deque<String> graph) {
        StringBuilder builder = new StringBuilder();
        int size = graph.size();
        int k = 0;
        for (String object : graph) {
            if (k++ < size - 1) {
                builder.append(object);
                builder.append(".");
            } else {
                builder.append(object);
            }
        }
        return builder.toString();
    }
}
